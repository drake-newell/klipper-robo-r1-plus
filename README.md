# klipper-robo-r1-plus

Klipper firmware printer.cfg for my Robo R1/R1+ with an E3D v6 hotend and T8*8 leadscrews. Based on Patrick Ma's config from the Robo3D forums (http://community.robo3d.com/index.php?threads/klipper.20202/).

